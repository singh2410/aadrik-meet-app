Instructions to run project in an editor:
1. Open a terminal inside this folder
2. Run - npm i && npm start (You need NodeJS v16.x installed on your system)
3. Select install through the CLI menu
4. Build for any supported platform
5. Enjoy.
That's all..
